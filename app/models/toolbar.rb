class Toolbar < ApplicationRecord
  belongs_to :ui
  has_many :toolbar_item_lists
  has_many :toolbar_items, through: :toolbar_item_lists
end
