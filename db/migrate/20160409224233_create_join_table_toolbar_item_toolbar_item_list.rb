class CreateJoinTableToolbarItemToolbarItemList < ActiveRecord::Migration[5.0]
  def change
    create_join_table :toolbar_items, :toolbar_item_lists do |t|
      # t.index [:toolbar_item_id, :toolbar_item_list_id]
      # t.index [:toolbar_item_list_id, :toolbar_item_id]
    end
  end
end
