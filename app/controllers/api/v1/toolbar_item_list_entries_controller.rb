class ToolbarItemListEntriesController < ApplicationController
  before_action :set_toolbar_item_list_entry, only: [:show, :update, :destroy]

  # GET /toolbar_item_list_entries
  def index
    @toolbar_item_list_entries = ToolbarItemListEntry.all

    render json: @toolbar_item_list_entries
  end

  # GET /toolbar_item_list_entries/1
  def show
    render json: @toolbar_item_list_entry
  end

  # POST /toolbar_item_list_entries
  def create
    @toolbar_item_list_entry = ToolbarItemListEntry.new(toolbar_item_list_entry_params)

    if @toolbar_item_list_entry.save
      render json: @toolbar_item_list_entry, status: :created, location: @toolbar_item_list_entry
    else
      render json: @toolbar_item_list_entry.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /toolbar_item_list_entries/1
  def update
    if @toolbar_item_list_entry.update(toolbar_item_list_entry_params)
      render json: @toolbar_item_list_entry
    else
      render json: @toolbar_item_list_entry.errors, status: :unprocessable_entity
    end
  end

  # DELETE /toolbar_item_list_entries/1
  def destroy
    @toolbar_item_list_entry.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_toolbar_item_list_entry
      @toolbar_item_list_entry = ToolbarItemListEntry.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def toolbar_item_list_entry_params
      params.require(:toolbar_item_list_entry).permit(:toolbar_item_id, :toolbar_item_list_id, :index)
    end
end
