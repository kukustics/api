class UiSerializer < ActiveModel::Serializer
  attributes  :id,
              :name,
              :toolbars

  def toolbars
    customized_toolbars = []

    object.toolbars.each do |toolbar|
      # Assign object attributes (returns a hash)
      # =========================================
      custom_toolbar = toolbar.slice :id, :snap, :x, :y

      # Custom nested and side-loaded attributes
      # =========================================
      # belongs_to
      customized_lists = []
      toolbar.toolbar_item_lists.each do |list|
        custom_list = list.slice :id
        customized_entries = []
        list.toolbar_item_list_entries.each do |entry|
          custom_entry = entry.slice :id, :index
          custom_entry[:toolbar_item] = entry.toolbar_item.slice :id, :icon, :description, :button_color, :stretch
          customized_entries.push(custom_entry)
        end
        custom_list[:toolbar_item_list_entries] = customized_entries
        customized_lists.push(custom_list)
      end
      custom_toolbar[:toolbar_item_lists] = customized_lists

      # =========================================
      customized_toolbars.push(custom_toolbar)
    end
    return customized_toolbars
  end
end
