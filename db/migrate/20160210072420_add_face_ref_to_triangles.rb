class AddFaceRefToTriangles < ActiveRecord::Migration[5.0]
  def change
    add_reference :triangles, :face, foreign_key: true
  end
end
