class Api::V1::ToolbarItemsController < ApplicationController
  before_action :set_toolbar_item, only: [:show, :update, :destroy]

  # GET /toolbar_items
  def index
    @toolbar_items = ToolbarItem.all

    render json: @toolbar_items
  end

  # GET /toolbar_items/1
  def show
    render json: @toolbar_item
  end

  # POST /toolbar_items
  def create
    @toolbar_item = ToolbarItem.new(toolbar_item_params)

    if @toolbar_item.save
      render json: @toolbar_item, status: :created, location: @toolbar_item
    else
      render json: @toolbar_item.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /toolbar_items/1
  def update
    if @toolbar_item.update(toolbar_item_params)
      render json: @toolbar_item
    else
      render json: @toolbar_item.errors, status: :unprocessable_entity
    end
  end

  # DELETE /toolbar_items/1
  def destroy
    @toolbar_item.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_toolbar_item
      @toolbar_item = ToolbarItem.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def toolbar_item_params
      params.fetch(:toolbar_item, {}).permit(
        :icon,
        :description
      )
    end
end
