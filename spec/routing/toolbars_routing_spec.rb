require "rails_helper"

RSpec.describe ToolbarsController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/toolbars").to route_to("toolbars#index")
    end

    it "routes to #new" do
      expect(:get => "/toolbars/new").to route_to("toolbars#new")
    end

    it "routes to #show" do
      expect(:get => "/toolbars/1").to route_to("toolbars#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/toolbars/1/edit").to route_to("toolbars#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/toolbars").to route_to("toolbars#create")
    end

    it "routes to #update" do
      expect(:put => "/toolbars/1").to route_to("toolbars#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/toolbars/1").to route_to("toolbars#destroy", :id => "1")
    end

  end
end
