# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
ui = Ui.create name: 'default'
toolbars = [
  ['top'],
  ['left'],
  ['right'],
  ['bottom']
]
toolbars.each_with_index do |toolbar, index|
  ui.toolbars << Toolbar.create(snap: toolbar[0])
end

#toolbar = Toolbar.create snap: "top"
#toolbar = Toolbar.create snap: "right"
#ui.toolbars << toolbar

toolbar_item_list = ToolbarItemList.new
ui.toolbars.first.toolbar_item_lists << toolbar_item_list
toolbar_item_list.save!

default_toolbar_items = [
  [ 'fa-music', 'kuku', false ],
  [ 'fa-eye', 'view', true ],
  [ 'fa-pencil', 'edit', true ],
  [ 'fa-volume-up', 'acoustics', true ],
  [ 'fa-wrench', 'tools', true ],
  [ 'fa-gear', 'settings', false ],
  [ 'fa-user', 'profile', false ],
]

default_toolbar_items.each_with_index do |item, index|
  icon = item[0]
  description = item[1]
  stretch = item[2]
  toolbar_item = ToolbarItem.create icon: icon, description: description, stretch: stretch
  entry = ToolbarItemListEntry.create index: index
  ui.toolbars.first.toolbar_item_lists.first.toolbar_item_list_entries << entry
  toolbar_item.toolbar_item_list_entries << entry
end

default_buttons = [
  ['sign_in', 'sign in'],
  ['sign_out', 'sign out'],
  ['sign_up', 'sign up']
]

user_list = [
  ['kuku', 'kukustics', 'kuku@gmail.com'],
  ['new_user', 'password', 'newbie@email.com'],
  ['old_user', 'password', 'oldie@email.com'],
  ['group_owner', 'password', 'group_owner@email.com'],
  ['premium_user', 'password', 'premium_user@email.com'],
  ['premium_group_owner', 'password', 'premium_group_user@email.com']
]
user_list.each do |username, password, email|
  User.create username: username, password: password, email: email
end

user = User.first
user.uis << ui
#user.save! password: 'kukustics'
