class CreateToolbarItemLists < ActiveRecord::Migration[5.0]
  def change
    create_table :toolbar_item_lists do |t|
    	t.references :toolbar, foreign_key: true
    	
      t.timestamps
    end
  end
end
