class ToolbarItemListSerializer < ApplicationSerializer
  attributes :id
  has_many :toolbar_item_list_entries
end
