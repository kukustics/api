class CreateToolbarItems < ActiveRecord::Migration[5.0]
  def change
    create_table :toolbar_items do |t|
      t.string :icon
      t.string :description
      t.string :button_color
      t.boolean :stretch

      t.timestamps
    end
  end
end
