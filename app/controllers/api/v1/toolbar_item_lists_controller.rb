class Api::V1::ToolbarItemListsController < ApplicationController
  before_action :set_toolbar_item_list, only: [:show, :update, :destroy]

  # GET /toolbar_item_lists
  def index
    @toolbar_item_lists = ToolbarItemList.all

    render json: @toolbar_item_lists
  end

  # GET /toolbar_item_lists/1
  def show
    render json: @toolbar_item_list
  end

  # POST /toolbar_item_lists
  def create
    @toolbar_item_list = ToolbarItemList.new(toolbar_item_list_params)

    if @toolbar_item_list.save
      render json: @toolbar_item_list, status: :created, location: @toolbar_item_list
    else
      render json: @toolbar_item_list.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /toolbar_item_lists/1
  def update
    if @toolbar_item_list.update(toolbar_item_list_params)
      render json: @toolbar_item_list
    else
      render json: @toolbar_item_list.errors, status: :unprocessable_entity
    end
  end

  # DELETE /toolbar_item_lists/1
  def destroy
    @toolbar_item_list.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_toolbar_item_list
      @toolbar_item_list = ToolbarItemList.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def toolbar_item_list_params
      params.fetch(:toolbar_item_list, {})
    end
end
