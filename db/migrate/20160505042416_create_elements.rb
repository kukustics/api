class CreateElements < ActiveRecord::Migration[5.0]
  def change
    create_table :elements do |t|
      t.string :name
      t.string :text
      t.string :type

      t.timestamps
    end
  end
end
