class VerticesController < ApplicationController
  before_action :set_vertex, only: [:show, :update, :destroy]

  # GET /vertices
  def index
    @vertices = Vertex.all

    render json: @vertices
  end

  # GET /vertices/1
  def show
    render json: @vertex
  end

  # POST /vertices
  def create
    @vertex = Vertex.new(vertex_params)

    if @vertex.save
      render json: @vertex, status: :created, location: @vertex
    else
      render json: @vertex.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /vertices/1
  def update
    if @vertex.update(vertex_params)
      render json: @vertex
    else
      render json: @vertex.errors, status: :unprocessable_entity
    end
  end

  # DELETE /vertices/1
  def destroy
    @vertex.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_vertex
      @vertex = Vertex.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def vertex_params
      params.require(:vertex).permit(:x, :y, :z)
    end
end
