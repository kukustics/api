class ToolbarItemListEntry < ApplicationRecord
  belongs_to :toolbar_item
  belongs_to :toolbar_item_list
end
