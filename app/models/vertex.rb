class Vertex < ApplicationRecord
  has_and_belongs_to_many :faces
  has_and_belongs_to_many :triangles

  validates :x, presence: true, numericality: true
  validates :y, presence: true, numericality: true
  validates :z, presence: true, numericality: true
end
