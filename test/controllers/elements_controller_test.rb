require 'test_helper'

class ElementsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @element = elements(:one)
  end

  test "should get index" do
    get elements_url
    assert_response :success
  end

  test "should create element" do
    assert_difference('Element.count') do
      post elements_url, params: { element: { name: @element.name, text: @element.text, type: @element.type } }
    end

    assert_response 201
  end

  test "should show element" do
    get element_url(@element)
    assert_response :success
  end

  test "should update element" do
    patch element_url(@element), params: { element: { name: @element.name, text: @element.text, type: @element.type } }
    assert_response 200
  end

  test "should destroy element" do
    assert_difference('Element.count', -1) do
      delete element_url(@element)
    end

    assert_response 204
  end
end
