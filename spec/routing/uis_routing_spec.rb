require "rails_helper"

RSpec.describe UisController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/uis").to route_to("uis#index")
    end

    it "routes to #new" do
      expect(:get => "/uis/new").to route_to("uis#new")
    end

    it "routes to #show" do
      expect(:get => "/uis/1").to route_to("uis#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/uis/1/edit").to route_to("uis#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/uis").to route_to("uis#create")
    end

    it "routes to #update" do
      expect(:put => "/uis/1").to route_to("uis#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/uis/1").to route_to("uis#destroy", :id => "1")
    end

  end
end
