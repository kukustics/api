class VertexSerializer < ActiveModel::Serializer
  attributes :id, :x, :y, :z
end
