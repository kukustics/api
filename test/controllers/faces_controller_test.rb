require 'test_helper'

class FacesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @face = faces(:one)
  end

  test "should get index" do
    get faces_url
    assert_response :success
  end

  test "should create face" do
    assert_difference('Face.count') do
      post faces_url, params: { face: { convex_vertices: @face.convex_vertices, reflex_vertices: @face.reflex_vertices, triangles: @face.triangles, vertices: @face.vertices } }
    end

    assert_response 201
  end

  test "should show face" do
    get face_url(@face)
    assert_response :success
  end

  test "should update face" do
    patch face_url(@face), params: { face: { convex_vertices: @face.convex_vertices, reflex_vertices: @face.reflex_vertices, triangles: @face.triangles, vertices: @face.vertices } }
    assert_response 200
  end

  test "should destroy face" do
    assert_difference('Face.count', -1) do
      delete face_url(@face)
    end

    assert_response 204
  end
end
