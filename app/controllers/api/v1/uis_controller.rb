class Api::V1::UisController < ApplicationController
  before_action :set_ui, only: [:show, :update, :destroy]

  # GET /uis
  def index
    @uis = Ui.all

    render json: @uis
  end

  # GET /uis/1
  def show
    render json: @ui
  end

  # POST /uis
  def create
    @ui = Ui.new(ui_params)

    if @ui.save
      render json: @ui, status: :created, location: @ui
    else
      render json: @ui.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /uis/1
  def update
    if @ui.update(ui_params)
      render json: @ui
    else
      render json: @ui.errors, status: :unprocessable_entity
    end
  end

  # DELETE /uis/1
  def destroy
    @ui.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ui
      @ui = Ui.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def ui_params
      params.fetch(:ui, {})
    end
end
