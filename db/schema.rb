# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160505065452) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "elements", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "text"
    t.string "type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "faces", id: :serial, force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "faces_vertices", id: false, force: :cascade do |t|
    t.bigint "face_id", null: false
    t.bigint "vertex_id", null: false
  end

  create_table "groups", id: :serial, force: :cascade do |t|
    t.string "name"
    t.string "logo"
    t.string "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "memberships", id: :serial, force: :cascade do |t|
    t.integer "group_id"
    t.integer "user_id"
    t.string "status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["group_id"], name: "index_memberships_on_group_id"
    t.index ["user_id"], name: "index_memberships_on_user_id"
  end

  create_table "rooms", id: :serial, force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "toolbar_item_list_entries", id: :serial, force: :cascade do |t|
    t.integer "toolbar_item_id"
    t.integer "toolbar_item_list_id"
    t.integer "index"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["toolbar_item_id"], name: "index_toolbar_item_list_entries_on_toolbar_item_id"
    t.index ["toolbar_item_list_id"], name: "index_toolbar_item_list_entries_on_toolbar_item_list_id"
  end

  create_table "toolbar_item_lists", id: :serial, force: :cascade do |t|
    t.integer "toolbar_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["toolbar_id"], name: "index_toolbar_item_lists_on_toolbar_id"
  end

  create_table "toolbar_item_lists_items", id: false, force: :cascade do |t|
    t.bigint "toolbar_item_id", null: false
    t.bigint "toolbar_item_list_id", null: false
  end

  create_table "toolbar_items", id: :serial, force: :cascade do |t|
    t.string "icon"
    t.string "description"
    t.string "button_color"
    t.boolean "stretch"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "toolbars", id: :serial, force: :cascade do |t|
    t.integer "ui_id"
    t.string "snap"
    t.integer "x"
    t.integer "y"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["ui_id"], name: "index_toolbars_on_ui_id"
  end

  create_table "toolbox_inclusions", id: :serial, force: :cascade do |t|
    t.integer "element_id"
    t.integer "toolbar_item_id"
    t.integer "index"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["element_id"], name: "index_toolbox_inclusions_on_element_id"
    t.index ["toolbar_item_id"], name: "index_toolbox_inclusions_on_toolbar_item_id"
  end

  create_table "triangles", id: :serial, force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "face_id"
    t.index ["face_id"], name: "index_triangles_on_face_id"
  end

  create_table "triangles_vertices", id: false, force: :cascade do |t|
    t.bigint "triangle_id", null: false
    t.bigint "vertex_id", null: false
  end

  create_table "uis", id: :serial, force: :cascade do |t|
    t.integer "group_id"
    t.integer "user_id"
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["group_id"], name: "index_uis_on_group_id"
    t.index ["user_id"], name: "index_uis_on_user_id"
  end

  create_table "users", id: :serial, force: :cascade do |t|
    t.string "username"
    t.string "website"
    t.string "first_name"
    t.string "last_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "provider", default: "email", null: false
    t.string "uid", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.string "email"
    t.json "tokens"
    t.string "invitation_token"
    t.datetime "invitation_created_at"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer "invitation_limit"
    t.string "invited_by_type"
    t.bigint "invited_by_id"
    t.integer "invitations_count", default: 0
    t.index ["email"], name: "index_users_on_email"
    t.index ["invitation_token"], name: "index_users_on_invitation_token", unique: true
    t.index ["invitations_count"], name: "index_users_on_invitations_count"
    t.index ["invited_by_id"], name: "index_users_on_invited_by_id"
    t.index ["invited_by_type", "invited_by_id"], name: "index_users_on_invited_by_type_and_invited_by_id"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["uid", "provider"], name: "index_users_on_uid_and_provider", unique: true
  end

  create_table "vertices", id: :serial, force: :cascade do |t|
    t.float "x"
    t.float "y"
    t.float "z"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "memberships", "groups"
  add_foreign_key "memberships", "users"
  add_foreign_key "toolbar_item_list_entries", "toolbar_item_lists"
  add_foreign_key "toolbar_item_list_entries", "toolbar_items"
  add_foreign_key "toolbar_item_lists", "toolbars"
  add_foreign_key "toolbars", "uis"
  add_foreign_key "toolbox_inclusions", "elements"
  add_foreign_key "toolbox_inclusions", "toolbar_items"
  add_foreign_key "triangles", "faces"
  add_foreign_key "uis", "groups"
  add_foreign_key "uis", "users"
end
