class ToolboxInclusion < ApplicationRecord
  belongs_to :element
  belongs_to :toolbar_item
end
