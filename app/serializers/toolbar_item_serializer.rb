class ToolbarItemSerializer < ActiveModel::Serializer
  attributes :id, :icon
end
