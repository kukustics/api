class Triangle < ApplicationRecord
  belongs_to :faces
  has_and_belongs_to_many :vertices
end
