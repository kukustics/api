class Element < ApplicationRecord
  has_many :toolbox_inclusions
  has_many :toolbar_items, through: :toolbox_inclusions
  # Unneccessary for column named default 'type'
  #self.inheritance_column = :type
  

  scope :tools, -> { where(type: 'Tool') }
  scope :buttons, -> { where(type: 'Button') }
  scope :links, -> { where(type: 'Link') }
  scope :cards, -> { where(type: 'Card') }

  def self.types
    %w(Tool Button Link Card)
  end
end
