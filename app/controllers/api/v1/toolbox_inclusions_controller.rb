class ToolboxInclusionsController < ApplicationController
  before_action :set_toolbox_inclusion, only: [:show, :update, :destroy]

  # GET /toolbox_inclusions
  def index
    @toolbox_inclusions = ToolboxInclusion.all

    render json: @toolbox_inclusions
  end

  # GET /toolbox_inclusions/1
  def show
    render json: @toolbox_inclusion
  end

  # POST /toolbox_inclusions
  def create
    @toolbox_inclusion = ToolboxInclusion.new(toolbox_inclusion_params)

    if @toolbox_inclusion.save
      render json: @toolbox_inclusion, status: :created, location: @toolbox_inclusion
    else
      render json: @toolbox_inclusion.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /toolbox_inclusions/1
  def update
    if @toolbox_inclusion.update(toolbox_inclusion_params)
      render json: @toolbox_inclusion
    else
      render json: @toolbox_inclusion.errors, status: :unprocessable_entity
    end
  end

  # DELETE /toolbox_inclusions/1
  def destroy
    @toolbox_inclusion.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_toolbox_inclusion
      @toolbox_inclusion = ToolboxInclusion.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def toolbox_inclusion_params
      params.require(:toolbox_inclusion).permit(:element_id, :toolbar_item_id, :index)
    end
end
