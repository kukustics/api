class CreateToolbarItemListEntries < ActiveRecord::Migration[5.0]
  def change
    create_table :toolbar_item_list_entries do |t|
      t.references :toolbar_item, foreign_key: true
      t.references :toolbar_item_list, foreign_key: true
      t.integer :index

      t.timestamps
    end
  end
end
