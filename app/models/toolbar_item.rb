class ToolbarItem < ApplicationRecord
  has_many :toolbar_item_list_entries
  has_many :toolbar_item_lists, through: :toolbar_item_list_entries
  has_many :toolbox_inclusions
  has_many :elements, through: :toolbox_inclusions
  
  delegate :tools, :buttons, :links, :cards, to: :elements
end
