class CreateJoinTableTriangleVertex < ActiveRecord::Migration[5.0]
  def change
    create_join_table :triangles, :vertices do |t|
      # t.index [:triangle_id, :vertex_id]
      # t.index [:vertex_id, :triangle_id]
    end
  end
end
