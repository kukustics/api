require 'test_helper'

class ToolboxInclusionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @toolbox_inclusion = toolbox_inclusions(:one)
  end

  test "should get index" do
    get toolbox_inclusions_url
    assert_response :success
  end

  test "should create toolbox_inclusion" do
    assert_difference('ToolboxInclusion.count') do
      post toolbox_inclusions_url, params: { toolbox_inclusion: { element_id: @toolbox_inclusion.element_id, index: @toolbox_inclusion.index, toolbar_item_id: @toolbox_inclusion.toolbar_item_id } }
    end

    assert_response 201
  end

  test "should show toolbox_inclusion" do
    get toolbox_inclusion_url(@toolbox_inclusion)
    assert_response :success
  end

  test "should update toolbox_inclusion" do
    patch toolbox_inclusion_url(@toolbox_inclusion), params: { toolbox_inclusion: { element_id: @toolbox_inclusion.element_id, index: @toolbox_inclusion.index, toolbar_item_id: @toolbox_inclusion.toolbar_item_id } }
    assert_response 200
  end

  test "should destroy toolbox_inclusion" do
    assert_difference('ToolboxInclusion.count', -1) do
      delete toolbox_inclusion_url(@toolbox_inclusion)
    end

    assert_response 204
  end
end
