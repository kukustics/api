require 'test_helper'

class ToolbarItemListEntriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @toolbar_item_list_entry = toolbar_item_list_entries(:one)
  end

  test "should get index" do
    get toolbar_item_list_entries_url
    assert_response :success
  end

  test "should create toolbar_item_list_entry" do
    assert_difference('ToolbarItemListEntry.count') do
      post toolbar_item_list_entries_url, params: { toolbar_item_list_entry: { index: @toolbar_item_list_entry.index, toolbar_item_id: @toolbar_item_list_entry.toolbar_item_id, toolbar_item_list_id: @toolbar_item_list_entry.toolbar_item_list_id } }
    end

    assert_response 201
  end

  test "should show toolbar_item_list_entry" do
    get toolbar_item_list_entry_url(@toolbar_item_list_entry)
    assert_response :success
  end

  test "should update toolbar_item_list_entry" do
    patch toolbar_item_list_entry_url(@toolbar_item_list_entry), params: { toolbar_item_list_entry: { index: @toolbar_item_list_entry.index, toolbar_item_id: @toolbar_item_list_entry.toolbar_item_id, toolbar_item_list_id: @toolbar_item_list_entry.toolbar_item_list_id } }
    assert_response 200
  end

  test "should destroy toolbar_item_list_entry" do
    assert_difference('ToolbarItemListEntry.count', -1) do
      delete toolbar_item_list_entry_url(@toolbar_item_list_entry)
    end

    assert_response 204
  end
end
