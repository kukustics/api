class ToolbarItemList < ApplicationRecord
  belongs_to :toolbar
  has_many :toolbar_item_list_entries
  has_many :toolbar_items, through: :toolbar_item_list_entries
end
