# kuku_api
API layer for Kukustics

Make sure rvm, ruby, and postgres are installed.

```gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3```

```\curl -sSL https://get.rvm.io | bash -s stable```

```sudo apt install postgresql```

You may need to install libpq-dev for the pg gem.

```sudo apt install libpq-dev```

libxml2 may be required for the libxml-ruby gem.

```sudo apt install libxml2```

Run a bundle install to install the gems (including rails).

```bundle install```

Use [this guide at DigitalOcean](https://www.digitalocean.com/community/tutorials/how-to-setup-ruby-on-rails-with-postgres) to get postgres up and running. In particular:

Switch to postgres user:

```$ su - postgres```

Open the postgres command line:

```$ psql```

Create development/test database role (use credentials in database.yml):

```# create role myapp with createdb login password 'password';```
