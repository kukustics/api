class CreateUis < ActiveRecord::Migration[5.0]
  def change
    create_table :uis do |t|
      t.references :group, foreign_key: true
      t.references :user, foreign_key: true

      t.string :name

      t.timestamps
    end
  end
end
