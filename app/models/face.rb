class Face < ApplicationRecord
  has_many :triangles
  has_and_belongs_to_many :vertices
end
