class User < ApplicationRecord
  devise :invitable, :database_authenticatable, :registerable,
          :recoverable, :rememberable, :trackable, :validatable,
          :omniauthable
  include DeviseTokenAuth::Concerns::User

  has_many :memberships
  has_many :groups, through: :memberships
  has_many :room_relationships
  has_many :rooms, through: :room_relationships
  has_many :uis

  #attr_accessor :username
  before_validation :set_provider
  before_validation :set_uid

  validates :username, presence: true
  validates :email, presence: true
  validates :password, presence: true, length: { minimum: 8 }

  private
  def set_provider
    self[:provider] = "email" if self[:provider].blank?
  end

  def set_uid
    self[:uid] = self[:email] if self[:uid].blank? && self[:email].present?
  end
end
