require "rails_helper"

RSpec.describe ToolbarItemListsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/toolbar_item_lists").to route_to("toolbar_item_lists#index")
    end

    it "routes to #new" do
      expect(:get => "/toolbar_item_lists/new").to route_to("toolbar_item_lists#new")
    end

    it "routes to #show" do
      expect(:get => "/toolbar_item_lists/1").to route_to("toolbar_item_lists#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/toolbar_item_lists/1/edit").to route_to("toolbar_item_lists#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/toolbar_item_lists").to route_to("toolbar_item_lists#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/toolbar_item_lists/1").to route_to("toolbar_item_lists#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/toolbar_item_lists/1").to route_to("toolbar_item_lists#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/toolbar_item_lists/1").to route_to("toolbar_item_lists#destroy", :id => "1")
    end

  end
end
