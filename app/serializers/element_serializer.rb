class ElementSerializer < ActiveModel::Serializer
  attributes :id, :name, :text, :type
end
