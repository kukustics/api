class TrianglesController < ApplicationController
  before_action :set_triangle, only: [:show, :update, :destroy]

  # GET /triangles
  def index
    @triangles = Triangle.all

    render json: @triangles
  end

  # GET /triangles/1
  def show
    render json: @triangle
  end

  # POST /triangles
  def create
    @triangle = Triangle.new(triangle_params)

    if @triangle.save
      render json: @triangle, status: :created, location: @triangle
    else
      render json: @triangle.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /triangles/1
  def update
    if @triangle.update(triangle_params)
      render json: @triangle
    else
      render json: @triangle.errors, status: :unprocessable_entity
    end
  end

  # DELETE /triangles/1
  def destroy
    @triangle.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_triangle
      @triangle = Triangle.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def triangle_params
      params.fetch(:triangle, {})
    end
end
