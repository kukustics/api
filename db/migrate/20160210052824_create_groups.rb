class CreateGroups < ActiveRecord::Migration[5.0]
  def change
    create_table :groups do |t|
      t.string :name
      t.string :logo
      t.string :status

      t.timestamps
    end
  end
end
