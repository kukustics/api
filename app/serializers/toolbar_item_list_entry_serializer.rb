class ToolbarItemListEntrySerializer < ApplicationSerializer
  attributes :id, :index
  has_one :toolbar_item
end
