class CreateJoinTableFaceVertex < ActiveRecord::Migration[5.0]
  def change
    create_join_table :faces, :vertices do |t|
      # t.index [:face_id, :vertex_id]
      # t.index [:vertex_id, :face_id]
    end
  end
end
