class CreateToolbars < ActiveRecord::Migration[5.0]
  def change
    create_table :toolbars do |t|
    	t.references :ui, foreign_key: true
    	
      t.string :snap
      t.integer :x
      t.integer :y

      t.timestamps
    end
  end
end
