Rails.application.routes.draw do
  resources :toolbox_inclusions
  resources :elements
  resources :toolbar_item_list_entries
  mount_devise_token_auth_for 'User', at: 'auth'
  namespace :api do
    api_version(
      module: "V1",
      header: {
        name: "Accept",
        value: "application/json; version=1"
      },
      defaults: { format: :json },
    ) do
      resources :toolbar_items
      resources :uis
      resources :toolbars
      resources :toolbar_item_lists
      resources :memberships
      resources :groups
      resources :users
      resources :rooms
      resources :faces
      resources :triangles
      resources :vertices
    end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  # Serve websocket cable requests in-process
  # mount ActionCable.server => '/cable'
end
