class CreateVertices < ActiveRecord::Migration[5.0]
  def change
    create_table :vertices do |t|
      t.float :x
      t.float :y
      t.float :z

      t.timestamps
    end
  end
end
