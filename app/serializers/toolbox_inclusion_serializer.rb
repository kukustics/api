class ToolboxInclusionSerializer < ActiveModel::Serializer
  attributes :id, :index
  has_one :element
  has_one :toolbar_item
end
