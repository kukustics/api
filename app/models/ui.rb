class Ui < ApplicationRecord
  belongs_to :user, optional: true
  has_many :toolbars
end
