class CreateToolboxInclusions < ActiveRecord::Migration[5.0]
  def change
    create_table :toolbox_inclusions do |t|
      t.references :element, foreign_key: true
      t.references :toolbar_item, foreign_key: true
      t.integer :index

      t.timestamps
    end
  end
end
