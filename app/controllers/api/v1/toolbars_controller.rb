class Api::V1::ToolbarsController < ApplicationController
  before_action :set_toolbar, only: [:show, :update, :destroy]

  # GET /toolbars
  def index
    @toolbars = Toolbar.all

    render json: @toolbars
  end

  # GET /toolbars/1
  def show
    render json: @toolbar
  end

  # POST /toolbars
  def create
    @toolbar = Toolbar.new(toolbar_params)

    if @toolbar.save
      render json: @toolbar, status: :created, location: @toolbar
    else
      render json: @toolbar.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /toolbars/1
  def update
    if @toolbar.update(toolbar_params)
      render json: @toolbar
    else
      render json: @toolbar.errors, status: :unprocessable_entity
    end
  end

  # DELETE /toolbars/1
  def destroy
    @toolbar.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_toolbar
      @toolbar = Toolbar.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def toolbar_params
      params.fetch(:toolbar, {})
    end
end
