class ToolbarSerializer < ActiveModel::Serializer
  attributes :id, :snap, :x, :y
  has_many :toolbar_item_lists
end
